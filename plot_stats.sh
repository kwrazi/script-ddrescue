#!/usr/bin/env bash
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

echo "A = [" > data.m
egrep '0x[0-9A-F]*\s*0x[0-9A-F]*' "$1" | sed -e 's/.$//' >> data.m
cat >> data.m <<EOF
];
A0 = A(find(A(:,2)<=1.5e6),2);
A1 = A(find(A(:,2)<=7.5e8),2);
A2 = A(find(A(:,2)>7.5e8 & A(:,2)<=1.5e9),2);
A3 = A(find(A(:,2)>1.5e9),2);

figure(1)
hist(A0,100);grid
figure(2)
hist(A1,100);grid
figure(3)
hist(A2,100);grid
figure(4)
hist(A3,100);grid

B2 = A(find(A(:,2)>7.5e8 & A(:,2)<=1.5e9),1);
C2 = diff(B2);
D2 = C2(find(C2 < 1.5e9));

figure(5)
hist(D2,50);grid
EOF

octave --persist data.m
