#!/usr/bin/env python2
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

import os
import sys
import signal
import subprocess
import time
import fcntl
import logging

import hdparm

def non_block_read(stream):
    fd = stream.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
    try:
        return stream.read()
    except:
        return ''

def display_process(subproc):
    out, err = subproc.communicate()
    sys.stdout.write(out)
    sys.stdout.write(err)
    sys.stdout.flush()

class ddrescue:

    __DEFAULT__ = { 
        'executable' : '/opt/git/ddrescue/ddrescue',
        'sector'     : 4096,
        'cluster'    : 256,
        'retries'    : 3,
        'src'        : '/dev/null',
        'dst'        : '/dev/null',
        'log'        : 'ddrescue.log',
        'force'      : False,
        'reverse'    : False,
        'quiet'      : True,
        'DEBUG'      : False,
        'FAKE'       : False}

    def __init__(self,**kwargs):
        if 'DEBUG' in kwargs and kwargs['DEBUG']:
            loglevel=logging.DEBUG
        else:
            loglevel=logging.NOTSET
        self.lg = logging.getLogger('ddrescue')
        self.lg.setLevel(loglevel)
        console = logging.StreamHandler()
        formatter = logging.Formatter(
            '%(name)-12s: %(levelname)-8s: %(message)s')
        console.setFormatter(formatter)
        self.lg.addHandler(console)
            
        self.lg.debug("Initializing...")
        # populate with default or kwargs
        for k,v in self.__DEFAULT__.items():
            if k in kwargs:
                setattr(self,k,kwargs[k])
            else:
                setattr(self,k,v)
            self.lg.debug("  %s = %s",k,getattr(self,k))
        # check for invalid argument
        for k,v in kwargs.items():
            if k not in self.__DEFAULT__:
                raise KeyError('%s not a valid argument' % k)
        self.popen = None
        self.serialno = dict()

    def buildcmd(self):
        self.cmd = list()
        self.lg.debug("Building command...")

        self.cmd.append(self.executable)
        if self.quiet:
            self.cmd.append('-q')
        self.cmd.append('-b%d' % (self.sector))
        self.cmd.append('-c%d' % (self.cluster))
        self.cmd.append('-r%d' % (self.retries))
        if self.reverse:
            self.cmd.append('-R')
        if self.force:
            self.cmd.append('-f')
        self.cmd.append(self.src)
        self.cmd.append(self.dst)
        self.cmd.append(self.log)
        self.check_serialno(self.src)
        if os.path.exists(self.dst):
            self.check_serialno(self.dst)
        # else: new file will be created
        
        self.lg.debug("  %s", ' '.join(self.cmd))

    def check_serialno(self,dev):
        hdp = hdparm.hdparm()
        # check if we have serial number of device
        #   if so, check that it hasn't changed
        # if not, save the serial number of device
        self.lg.debug("Checking device '%s'...", dev)
        if dev in self.serialno:
            # have serial number
            try: 
                hdp.getinfo(dev)
            except:
                raise OSError('FATAL: Device has been removed.')

            hdp_serialno = hdp.info['SerialNo']
            dev_serialno = self.serialno[dev]
            if dev_serialno == hdp_serialno:
                self.lg.debug("  serialno[%s] == %s.", dev,hdp_serialno)
            else:
                raise OSError('FATAL: SerialNo has changed. "%s"!="%s"'
                              % (dev_serialno,hdp_serialno))
        else:
            # save serial number
            if hdp.is_blockdev(dev):
                try: 
                    hdp.getinfo(dev)
                except:
                    self.lg.debug("  No serial number found.")
                    return None
                self.serialno[dev] = hdp.info['SerialNo']
                self.lg.debug("  serialno[%s] = %s",dev,hdp.info['SerialNo'])
            else:
                self.lg.debug('  Not a block device.')

    def isrunning(self):
        if self.popen is None:
            return False
        try:
            os.kill(self.popen.pid,0)
        except OSError, err:
            if err.errno == os.errno.ESRCH:
                # print('No running process found..')
                return False
            elif err.errno == os.errno.EPERM:
                # print('No permission to signal this process!')
                return False
            else:
                # print('Unknown error.')
                return False
        else:
            return True

    def end_after(self,delay):
        # kill running ddrescue after delay  
        self.lg.debug('Killing process after %d secs...',delay)
        timeleft = delay
        while timeleft > 0:
            nextline = non_block_read(self.popen.stdout)
            errline = non_block_read(self.popen.stderr)
            if nextline == '' and errline == '' and self.popen.poll() != None:
                break
            sys.stdout.write(nextline)
            sys.stdout.write(errline)
            sys.stdout.flush()
            time.sleep(1)
            timeleft -= 1

        if self.popen.poll() != None:
            # process has exit. This is not expected.
            raise OSError(2,"Process has exited unexpectedly.",self.cmd[0])
        else:
            pid = self.popen.pid
            for i in xrange(5):
                self.lg.debug('Stopping process %d with SIGINT...',pid)
                # kill(pid,signal.SIGINT)
                self.popen.send_signal(signal.SIGINT)
                display_process(self.popen)
                time.sleep(2)
                if not self.isrunning():
                    break
            if self.isrunning():
                self.lg.debug("  Process did not die. Using SIGTERM")
                # kill(pid,signal.SIGTERM)
                self.popen.send_signal(signal.SIGTERM)
                display_process(self.popen)
                time.sleep(5)
                if self.isrunning():
                    self.lg.debug("  Process did not die. Using SIGKILL")
                    # kill(pid,signal.SIGKILL)
                    self.popen.send_signal(signal.SIGKILL)
                    display_process(self.popen)
                    time.sleep(5)
                    if self.isrunning():
                        self.lg.debug("  Process did not die. Final try.")
                        self.popen.terminate()
                        self.popen.wait()
        
    def start(self,**kwargs):
        # populate with default or kwargs
        for k,v in self.__DEFAULT__.items():
            if k in kwargs:
                setattr(self,k,kwargs[k])

        if self.FAKE:
            # use wget to test subprocess start and end_after
            self.lg.debug('debug mode found -- using wget subprocess.')
            url = 'http://mirror.aarnet.edu.au/pub/OpenBSD/5.4/amd64/install54.iso'
            self.cmd = ['/usr/bin/wget','-c','--limit-rate=1k',url]
            self.lg.debug("  %s",' '.join(self.cmd))
        else:
            self.buildcmd()

        # start the subprocess
        self.popen = subprocess.Popen(
            self.cmd,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # wait for 2 seconds for process to start
        time.sleep(2)
        if self.popen.poll() is None:
            self.lg.debug('Process "%s" has started...',self.cmd[0])
        else:
            display_process(self.popen)
            raise OSError(self.popen.returncode,"Process could not be started.",self.cmd[0])

if __name__ == "__main__":

    ddr = ddrescue(executable='ddrescue',src='ddrescue.py',dst='output.dat',log='test.log',DEBUG=False,FAKE=True)
    src = ddr.src
    if not os.path.exists(src):
        raise OSError("%s does not exists" % (src))
    ddr.buildcmd()
    ddr.check_serialno('/dev/sda')
    ddr.check_serialno('/dev/sdb')
    ddr.check_serialno('/dev/null')
    ddr.check_serialno('test_ddrescue.log')
    try:
        ddr.check_serialno('/dev/sdz')
    except:
        print("Good: Failed as expected.")
    else:
        print("Bad: Failure expected -- Please check.")
    ddr.start()
    ddr.end_after(30)
