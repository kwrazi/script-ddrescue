#!/usr/bin/env python2
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

import subprocess
import os
import stat

class hdparm:

    def __init__(self):
        self.info = dict()
        self.device = None
        
    def is_blockdev(self,device):
        if os.path.exists(device):
            mode = os.stat(device).st_mode
            return stat.S_ISBLK(mode)
        else:
            raise OSError(3,'File not found.',device)

    def getinfo(self,device):
        self.info = dict()
        self.device = None
        if not self.is_blockdev(device):
            raise OSError(1,'Not a block device',device)
        cmd = ['hdparm','-i',device]
        p = subprocess.Popen(cmd, 
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        out, err = p.communicate()
        if p.returncode == 0 or p.returncode == 25: 
            self.device = device
            for line in out.splitlines():
                tok = line.strip().split(', ')
                for c in tok:
                    a = c.split('=',1)
                    if len(a) == 2:
                        self.info[a[0]] = a[1]
        else:
            raise IOError(p.returncode,'Could not run hdparm.',device)
        return self.info

    def showinfo(self):
        print("---- %s ----" % (self.device))
        for k,v in self.info.iteritems():
            print('  %s=%s' % (k,v))

if __name__ == "__main__":

    # Note: You might need to run this as root.

    # Test hdparm
    hdinfo = hdparm()
    hdinfo.showinfo()
    hdinfo.getinfo('/dev/sda')
    hdinfo.showinfo()
    hdinfo.getinfo('/dev/sdb')
    hdinfo.showinfo()
    print('----')
    try:
        hdinfo.getinfo('/dev/null')
    except OSError, e:
        print("Failed as expected. Code=%d, Msg='%s'" % (e.errno,e[1]))
        
