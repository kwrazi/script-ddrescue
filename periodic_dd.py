#!/usr/bin/env python2
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

import os
import sys
import argparse

import ddrescuelog
import ddrescue

def argv_parse():
    parser = argparse.ArgumentParser(
        description='Control ddrescue for stride copying.')
    parser.add_argument('--version',action='version',version='%(prog)s 0.5')
    parser.add_argument('-s','--src', required=True, 
                        help='source filename or device')
    parser.add_argument('-d','--dst', required=True, 
                        help='destination filename or device')
    parser.add_argument('-l','--log', required=True,
                        help='ddrescuelog filename')
    parser.add_argument('--block', default=4096, type=int,
                        help='ddresue block size in bytes (default: 4096)')
    parser.add_argument('--cluster', default=256, type=int,
                        help='cluster size in blocks (default: 256)')
    parser.add_argument('--retries', default=3, type=int,
                        help='ddrescue error retries (default: 3)')
    parser.add_argument('--stride', default=1048576, type=int,
                        help='stride spacing in bytes (default: 1048576)')
    args = parser.parse_args()
    if not os.path.exists(args.log):
        print('Error: ddrescue log file "%s" not found.' % (args.log))
        sys.exit()
    if args.stride <= args.block:
        print('Error: stride spacing %d <= %d.' % (args.stride, args.block))
        sys.exit()
    return args

if __name__ == "__main__":

    args = argv_parse()

    log = ddrescuelog.ddrescuelog(args.log,DEBUG=True)
    log.read_file()
    ddr = ddrescue.ddrescue(
        src=args.src, dst=args.dst, log=log.file, 
        sector=args.block, cluster=args.cluster, DEBUG=True)

    print("Using initial stride size = %d" % (args.stride))

    offset = log.position_add(args.stride)
    log.write_file()

    ddr.start(reverse=True)
    ddr.end_after(60)
    log.read_file()
    
    log.position_set(offset)
    
    log.write_file()
    ddr.start(reverse=False)
    ddr.end_after(60)
    log.read_file()

    sys.exit()

    while True:
        offset = log.position_add(args.stride)
        
        log.write_file()
        ddr.start(reverse=True)
        ddr.end_after(60)
        log.read_file()
        
        log.position_set(offset)
        
        log.write_file()
        ddr.start(reverse=False)
        ddr.end_after(60)
        log.read_file()
