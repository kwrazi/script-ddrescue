Introduction
------------

Utility scripts used with ddrescue.

Plot Statistics
---------------

=== plot_stats.sh <ddrescue_log>

  Shell script to read ddrescue log files and create histograms of the
  of the data. Octave is required.

=== hdparm.py

  Python module to use hdparm to get the serial number of the
  harddisk. The main reason for this is my system does not guarantee
  the drives will remain fixed, and could be removed without warning.

  It's also good for systems where there is a large number of /dev/sd?
  device and they may move around between power-cycles. This way, you
  can give serial numbers to prevent accidental overwrite to wrong
  drive.

=== ddrescuelog.py

  Python module to read and write ddrescuelog.

=== ddrescue.py

  Python module to manage ddrescue subprocess.

=== periodic_dd.py

  Python script to do stride ddrescue.
