#!/usr/bin/env python2
#
# Kiet To <kwrazi@gmail.com>
# December 2013
#

import sys
import logging

class ddrescuelog:

    __DEFAULT__ = {
        'comment' : ['# Rescue Logfile. Created by ddrescuelog.py\n', 
                     '# Command line: None\n', 
                     '# current_pos  current_status\n', 
                     '#      pos        size  status\n'], 
        'current' : (0,'?') }
    
    def __init__(self,filename,**kwargs):
        if 'DEBUG' in kwargs and kwargs['DEBUG']:
            loglevel=logging.DEBUG
        else:
            loglevel=logging.NOTSET
        self.lg = logging.getLogger('ddrescuelog')
        self.lg.setLevel(loglevel)
        console = logging.StreamHandler()
        formatter = logging.Formatter(
            '%(name)-12s: %(levelname)-8s: %(message)s')
        console.setFormatter(formatter)
        self.lg.addHandler(console)

        self.file = filename
        self.reset()

    def default(self,size=1024):
        # populate with default or kwargs
        self.lg.debug("Setting defaults...")
        for k,v in self.__DEFAULT__.items():
            setattr(self,k,v)
            self.lg.debug("  %s = %s",k,v)
        self.data = [(0,size,'?')]

    def reset(self):
        self.comment = list()
        self.current = (0,'?')
        self.data = list()

    def parse_line(self,line):
        if line[0] == '0':
            l = line.split()
            if len(l) == 3:
                self.data.append( (int(l[0],0), int(l[1],0), l[2]) )
            elif len(l) == 2:
                self.current = (int(l[0],0), l[1]) 
        elif line[0] == '#':
            self.comment.append(line)

    def read_file(self,filename=None):
        if filename is None:
            filename = self.file
        else:
            self.file = filename
        self.lg.debug("Reading logfile %s.",filename)
        f = open(filename,'r')
        lcount = 0
        self.reset()
        while True:
            lines = f.readlines(100000)
            if not lines:
                break
            for line in lines:
                self.parse_line(line)
                lcount += 1
        f.close()
        pos = self.position()
        size = self.disksize()
        self.lg.debug("  Current position - 0x%08X (%d)",pos,pos)
        self.lg.debug("  disksize = 0x%08X (%d)",size,size)
        self.lg.debug("  %d lines read.",lcount)

    def write_log(self):
        filename = self.file
        f = open(filename,'w')
        lcount = 0
        # write header
        for i in xrange(len(self.comment)-1):
            f.write(self.comment[i])
            lcount += 1
        # write current offset
        f.write("0x%08X     %s\n" % self.current)
        f.write(self.comment[-1])
        lcount += 2
        for t in self.data:
            f.write("0x%08X  0x%08X  %s\n" % t)
            lcount += 1
        f.close()
        self.lg.debug("  %d lines written.",lcount)

    def write_file(self,filename=None):
        if filename is None:
            filename = self.file
        else:
            self.file = filename
        self.lg.debug("Writing logfile %s.",filename)
        self.write_log()
        self.lg.debug("  Current position - 0x%08X", self.current[0])

    def create_new(self,filename,size):
        self.file = filename
        self.lg.debug("Creating new logfile %s.", filename)
        f = open(filename,'w')
        lcount = 0
        # create blank data
        self.default(size)
        # write file
        self.write_log()
        self.lg.debug("  Current position - 0x%08X", self.current[0])

    def position_add(self,skipbytes):
        # modify ddrescuelog initial offset by "skipbytes"
        align_bytes = int(skipbytes / 4096) * 4096
        pos = ( self.current[0] + align_bytes ) % self.disksize()
        self.current = (pos, self.current[1] )
        self.lg.debug("Current position + 0x%08X  = 0x%08X (%d)", 
                      skipbytes,pos,pos)
        return self.position()

    def position(self):
        # set the ddrescuelog initial offset to "offset"
        return self.current[0]

    def position_set(self,offset):
        # set the ddrescuelog initial offset to "offset"
        self.current = ( offset, self.current[1] )
        self.lg.debug("New position 0x%08X (%d)", offset, offset)

    def disksize(self):
        lastentry = self.data[-1];
        size = lastentry[0] + lastentry[1]
        return size

    def data_at(self,offset):
        for t in self.data:
            start = t[0]
            end = t[0] + t[1]
            if offset >= start and offset < end:
                return t

    def dump(self):
        # print to stdout
        # write header
        print('--- data dump: begin ---')
        for i in xrange(len(self.comment)-1):
            sys.stdout.write(self.comment[i])
        # write current offset
        sys.stdout.write("0x%08X     %s\n" % self.current)
        sys.stdout.write(self.comment[-1])
        for t in self.data:
            sys.stdout.write("0x%08X  0x%08X  %s\n" % t)
        sys.stdout.flush()
        print('--- data dump: end -----')

if __name__ == "__main__":

    import os
    import subprocess

    log = ddrescuelog('test_ddrescue.log',DEBUG=True)
    log.read_file()
    offset = log.position_add(0.75e9)
    log.write_file('new_ddrescue.log')

    print('==== comparing test_ddrescue.log and new_ddrescue.log ====')
    subprocess.call(['diff','test_ddrescue.log','new_ddrescue.log'])

    print('==== testing data_at() ====')
    olist = [ 3267380224, 416222875648, 0, 412099936256 ]
    for offset in olist:
        print("Looking for block at %d" % (offset))
        print("  0x%08X 0x%08X %s" % log.data_at(offset))
        print("  %d %d %s" % log.data_at(offset))

    print('==== testing create_new() ====')
    log.create_new('dummy.log',1024*1024*1024*1024)
    log.dump()
    
    print('==== cleaning up ====')
    os.remove('dummy.log')
    os.remove('new_ddrescue.log')
